module.exports = {
  type: 'transform',
  esbuild: {
    target: 'esnext',
    loader: {
      '.gql': 'text',
    },
  },
};
