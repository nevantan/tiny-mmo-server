module default {
  scalar type CharacterId extending sequence;

  type User {
    required property email -> str {
      constraint exclusive;
    }
    required property password -> str;

    multi link characters := .<player[is Character]
  }

  type Character {
    required property uid -> CharacterId {
      constraint exclusive;
    }

    required property name -> str;
    required property tag -> int16 {
      constraint min_value(0);
      constraint max_value(9999);
    }
    constraint exclusive on((.name, .tag));

    required property positionX -> int16;
    required property positionY -> int16;

    required property zone -> str;

    required link player -> User;
  }
}
