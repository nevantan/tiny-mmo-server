CREATE MIGRATION m1edcywsq3qbmtn7s37kwvq3dvi6wikrebr7tryxyzpclopknpo7oq
    ONTO m1czxczezijbznu4h347cj3y5lguvkscoqi3mpvzkjsybznbg4obaq
{
  ALTER TYPE default::Character {
      CREATE REQUIRED PROPERTY scene -> std::str {
          SET REQUIRED USING ('Playground');
      };
  };
};
