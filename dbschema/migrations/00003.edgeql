CREATE MIGRATION m1mgbzfbv2r2rcltuyofln524kfaar7fcx4dmopoqfwlb3c55g4zva
    ONTO m1edcywsq3qbmtn7s37kwvq3dvi6wikrebr7tryxyzpclopknpo7oq
{
  ALTER TYPE default::Character {
      ALTER PROPERTY scene {
          RENAME TO zone;
      };
  };
};
