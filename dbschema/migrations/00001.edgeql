CREATE MIGRATION m1czxczezijbznu4h347cj3y5lguvkscoqi3mpvzkjsybznbg4obaq
    ONTO initial
{
  CREATE SCALAR TYPE default::CharacterId EXTENDING std::sequence;
  CREATE TYPE default::Character {
      CREATE REQUIRED PROPERTY name -> std::str;
      CREATE REQUIRED PROPERTY tag -> std::int16 {
          CREATE CONSTRAINT std::max_value(9999);
          CREATE CONSTRAINT std::min_value(0);
      };
      CREATE CONSTRAINT std::exclusive ON ((.name, .tag));
      CREATE REQUIRED PROPERTY positionX -> std::int16;
      CREATE REQUIRED PROPERTY positionY -> std::int16;
      CREATE REQUIRED PROPERTY uid -> default::CharacterId {
          CREATE CONSTRAINT std::exclusive;
      };
  };
  CREATE TYPE default::User {
      CREATE REQUIRED PROPERTY email -> std::str {
          CREATE CONSTRAINT std::exclusive;
      };
      CREATE REQUIRED PROPERTY password -> std::str;
  };
  ALTER TYPE default::Character {
      CREATE REQUIRED LINK player -> default::User;
  };
  ALTER TYPE default::User {
      CREATE MULTI LINK characters := (.<player[IS default::Character]);
  };
};
