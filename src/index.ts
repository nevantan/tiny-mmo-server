// Polyfill
import 'isomorphic-fetch';

// Libraries
import express from 'express';
import { createServer } from 'http';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import { json } from 'body-parser';
import cors from 'cors';
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import jwt from 'jsonwebtoken';
import { RedisPubSub } from 'graphql-redis-subscriptions';

// GraphQL
import typeDefs from './graphql/schema.gql';
import { resolvers } from './graphql/resolvers';

// Services
import { CharacterService } from './services/Character';
import { LoginService } from './services/Login';
import { UserService } from './services/User';

// Controllers
import { GameController } from './controllers/Game';
import { MobController } from './controllers/Mob';
import { PlayerController } from './controllers/Player';
import { ZoneController } from './controllers/Zone';

const game = new GameController();
const mc = new MobController({ id: 0, game, capacity: 20 });
const pc = new PlayerController(game);
const zc = new ZoneController();

export interface TinyMMOContext {
  redis: RedisPubSub;
  user: {
    id: string;
  } | null;
  dataSources: {
    character: CharacterService;
    login: LoginService;
    user: UserService;
  };
}

const app = express();
const httpServer = createServer(app);

// Build schema and Apollo server
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
const server = new ApolloServer<TinyMMOContext>({
  schema,
  plugins: [
    ApolloServerPluginDrainHttpServer({ httpServer }),
    {
      async serverWillStart() {
        return {
          async drainServer() {
            await serverCleanup.dispose();
          },
        };
      },
    },
  ],
});

// Setup WS server
const wss = new WebSocketServer({
  server: httpServer,
  path: '/graphql',
});

// Hook in WS server to the GraphQL server
const serverCleanup = useServer(
  {
    schema,
    context: async ({ connectionParams }, msg, args) => {
      let decoded;

      try {
        if (connectionParams?.authorization) {
          decoded = jwt.verify(
            (connectionParams?.authorization as string).split(' ')[1],
            process.env.JWT_SECRET ?? ''
          );
        }
      } catch (e: any) {
        console.log('Invalid token (subscription)');
      }

      return {
        redis: new RedisPubSub(),
        user: decoded ? { id: decoded.sub as string } : null,
        dataSources: {
          character: new CharacterService(),
          login: new LoginService(),
          user: new UserService(),
        },
      };
    },
  },
  wss
);

server.start().then(() => {
  app.use(
    '/graphql',
    cors<cors.CorsRequest>(),
    json(),
    expressMiddleware(server, {
      context: async ({ req }) => {
        let decoded;

        try {
          if (req.headers.authorization)
            decoded = jwt.verify(
              req.headers.authorization.split(' ')[1],
              process.env.JWT_SECRET ?? ''
            );
        } catch (e: any) {
          console.log('Invalid token');
        }

        return {
          redis: new RedisPubSub(),
          user: decoded
            ? {
                id: decoded.sub as string,
              }
            : null,
          dataSources: {
            character: new CharacterService(),
            login: new LoginService(),
            user: new UserService(),
          },
        };
      },
    })
  );

  const port = 4000;
  httpServer.listen(port, () => {
    console.log(`GraphQL listening on port ${port}...`);
  });
});
