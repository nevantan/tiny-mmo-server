export enum Direction {
  Left = -1,
  Right = 1,
  Up = -1,
  Down = 1,
}

export interface Facing {
  x: number;
  y: number;
}
