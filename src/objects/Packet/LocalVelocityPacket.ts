import { Vector } from '../../util/Vector';
import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';

export class LocalVelocityPacket extends Packet {
  constructor(protected _velocity: Vector) {
    super(PacketType.LocalFacing);
  }

  public serialize() {
    const packet = Buffer.alloc(3);
    packet.writeUInt8(this._type);
    packet.writeInt8(this._velocity.x, 1);
    packet.writeInt8(this._velocity.y, 2);

    return packet;
  }

  get velocity() {
    return this._velocity;
  }

  static parse(data: Buffer) {
    try {
      const x = data.readInt8(0);
      const y = data.readInt8(1);
      return new LocalVelocityPacket(new Vector(x, y));
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
