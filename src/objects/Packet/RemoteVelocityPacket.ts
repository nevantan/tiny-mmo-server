// Libraries
import { Packet } from './Packet';
import { Vector } from '../../util/Vector';

// Types
import { PacketType } from './PacketType';

export class RemoteVelocityPacket extends Packet {
  constructor(
    protected _uid: number,
    protected _velocity: Vector,
    protected _position: Vector
  ) {
    super(PacketType.RemoteVelocity);
  }

  public serialize() {
    const packet = Buffer.alloc(11);
    packet.writeUInt8(this._type);
    packet.writeUInt32BE(this._uid, 1);
    packet.writeInt8(this._velocity.x, 5);
    packet.writeInt8(this._velocity.y, 6);
    packet.writeInt16BE(this._position.x, 7);
    packet.writeInt16BE(this._position.y, 9);

    return packet;
  }

  get uid() {
    return this._uid;
  }

  get velocity() {
    return this._velocity;
  }

  get position() {
    return this._position;
  }

  static parse(data: Buffer) {
    try {
      const uid = data.readInt32BE();

      const vx = data.readInt8(4);
      const vy = data.readInt8(5);

      const px = data.readInt16BE(6);
      const py = data.readInt16BE(8);

      return new RemoteVelocityPacket(
        uid,
        new Vector(vx, vy),
        new Vector(px, py)
      );
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
