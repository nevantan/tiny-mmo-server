import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';
import { Facing } from '../../enums/Direction';

export class RemoteFacingPacket extends Packet {
  constructor(protected _facing: Facing, protected _uid: number) {
    super(PacketType.RemoteFacing);
  }

  public serialize() {
    const packet = Buffer.alloc(7);
    packet.writeUInt8(this._type);
    packet.writeInt8(this._facing.x, 1);
    packet.writeInt8(this._facing.y, 2);
    packet.writeUInt32BE(this._uid, 3);

    return packet;
  }

  get facing() {
    return this._facing;
  }

  get uid() {
    return this._uid;
  }

  static parse(data: Buffer) {
    try {
      return new RemoteFacingPacket(
        {
          x: data.readInt8(0),
          y: data.readInt8(1),
        },
        data.readUInt32BE(2)
      );
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
