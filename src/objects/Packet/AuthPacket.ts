import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class AuthPacket extends Packet {
  constructor(protected _token: string) {
    super(PacketType.Auth);
  }

  public serialize() {
    const type = Buffer.alloc(1);
    type.writeUInt8(this._type);

    const payload = Buffer.from(this._token, 'utf8');

    return Buffer.concat([type, payload], type.length + payload.length);
  }

  get token() {
    return this._token;
  }

  static parse(data: Buffer) {
    try {
      return new AuthPacket(data.toString('utf8'));
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
