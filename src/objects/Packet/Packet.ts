import { PacketType } from './PacketType';

export class Packet {
  constructor(protected _type: PacketType) {}

  serialize() {
    return Buffer.alloc(0);
  }

  get type() {
    return this._type;
  }

  static parse(data: Buffer) {
    const type = data.readUInt8(0);
    const payload = data.subarray(1);

    throw new Error(`Unsupported packet of type ${type}: ${payload}`);
  }
}
