import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class KeepAlivePacket extends Packet {
  constructor(protected _ping: boolean) {
    super(PacketType.KeepAlive);
  }

  public serialize() {
    const packet = Buffer.alloc(2);
    packet.writeUInt8(this._type);
    packet.writeUInt8(this._ping ? 1 : 0, 1);

    return packet;
  }

  get ping() {
    return this._ping;
  }

  static parse(data: Buffer) {
    try {
      return new KeepAlivePacket(data.readUInt8() === 1 ? true : false);
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
