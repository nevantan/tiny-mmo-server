import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class AuthSuccessPacket extends Packet {
  constructor() {
    super(PacketType.AuthSuccess);
  }

  public serialize() {
    const type = Buffer.alloc(1);
    type.writeUInt8(this._type);

    return type;
  }

  static parse(data: Buffer) {
    try {
      return new AuthSuccessPacket();
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
