import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class CharacterSelectedPacket extends Packet {
  constructor(protected _uid: number) {
    super(PacketType.CharacterSelected);
  }

  public serialize() {
    // [Type, UID, UID, UID, UID, Tag, Tag, X, X, Y, Y, Name Len]
    const packet = Buffer.alloc(5);
    packet.writeUInt8(this._type);
    packet.writeUInt32BE(this._uid, 1);

    return packet;
  }

  get uid() {
    return this._uid;
  }

  static parse(data: Buffer) {
    try {
      const uid = data.readUInt32BE(0);

      return new CharacterSelectedPacket(uid);
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
