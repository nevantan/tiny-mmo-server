import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';
import { Facing } from '../../enums/Direction';

export class LocalFacingPacket extends Packet {
  constructor(protected _facing: Facing) {
    super(PacketType.LocalFacing);
  }

  public serialize() {
    const packet = Buffer.alloc(3);
    packet.writeUInt8(this._type);
    packet.writeInt8(this._facing.x, 1);
    packet.writeInt8(this._facing.y, 2);

    return packet;
  }

  get facing() {
    return this._facing;
  }

  static parse(data: Buffer) {
    try {
      return new LocalFacingPacket({
        x: data.readInt8(0),
        y: data.readInt8(1),
      });
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
