import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class SelectCharacterPacket extends Packet {
  constructor(protected _characterUID: number) {
    super(PacketType.SelectCharacter);
  }

  public serialize() {
    const packet = Buffer.alloc(5);
    packet.writeUInt8(this._type);
    packet.writeUInt32BE(this._characterUID, 1);

    return packet;
  }

  get uid() {
    return this._characterUID;
  }

  static parse(data: Buffer) {
    try {
      return new SelectCharacterPacket(data.readUInt32BE());
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
