// Libraries
import { Vector } from '../../util/Vector';
import Redis from 'ioredis';

// Types
import { Direction, Facing } from '../../enums/Direction';

// Utils
import { tagToString } from '../../util/tagToString';

const SCALING_FACTOR = 4;

export interface CharacterConfig {
  id: string;
  uid: number;
  name: string;
  tag: number;
  taggedName?: string;
  position?: {
    x: number;
    y: number;
  };
  positionX?: number;
  positionY?: number;
  zone: string;
}

export class Character {
  private _redis = new Redis();
  private _subscriptions = new Redis();
  private _speed = 200;

  private _id: string;
  private _uid: number;
  private _name: string;
  private _tag: number;
  private _taggedName: string;
  private _position = new Vector(0, 0);
  private _zone: string;

  private _mapInfo: {
    map_width: number;
    map_height: number;
    tile_size: number;
  } = { map_width: 0, map_height: 0, tile_size: 8 };
  private _collider = {
    x: -12,
    y: -12,
    width: 24,
    height: 24,
  };

  private _velocity = new Vector(0, 0);
  private _facing: Facing = {
    x: Direction.Right,
    y: Direction.Down,
  };

  constructor(args: CharacterConfig) {
    this._id = args.id;
    this._uid = args.uid;
    this._name = args.name;
    this._tag = args.tag;
    this._taggedName =
      args.taggedName ?? `${args.name}#${tagToString(args.tag)}`;
    this._position = new Vector(
      args.position?.x ?? args.positionX ?? 0,
      args.position?.y ?? args.positionY ?? 0
    );
    this._zone = args.zone;

    // Pull some initial map data from redis
    this._redis.hgetall(`zone:${this._zone}:info`).then((map) => {
      this._mapInfo = {
        map_width: parseInt(map.map_width),
        map_height: parseInt(map.map_height),
        tile_size: parseInt(map.tile_size) * SCALING_FACTOR,
      };
    });
  }

  public async update(dt: number, tick: number) {
    let velocity = new Vector(0, 0);
    if (this._velocity.size > 0) velocity = this._velocity.normalize();

    const speed = this._speed * dt;
    const stepCount = 2;
    const stepLength = speed / stepCount;

    // Simulate steps one at a time
    for (let i = 0; i < stepCount; i++) {
      // Save our previous position
      const prevPos = new Vector(this._position.x, this._position.y);

      // Simulate X movement
      this._position.x += velocity.x * stepLength;

      // Test collision
      const collidedX = await this._testCollision(
        new Vector(
          this._position.x + this._collider.x,
          this._position.y + this._collider.y
        )
      );
      // If there was a collision, revert the X movement
      if (collidedX) this._position.x = prevPos.x;

      // Simulate Y movement
      this._position.y += velocity.y * stepLength;

      // Test collision
      const collidedY = await this._testCollision(
        new Vector(
          this._position.x + this._collider.x,
          this._position.y + this._collider.y
        )
      );
      // If there was a collision, revert the Y movement
      if (collidedY) this._position.y = prevPos.y;
    }
  }

  public serialize() {
    return {
      id: this._id,
      uid: this._uid,
      name: this._name,
      tag: this._tag,
      positionX: this._position.x,
      positionY: this._position.y,
      facingX: this._facing.x,
      facingY: this._facing.y,
      velocityX: this._velocity.x,
      velocityY: this._velocity.y,
      zone: this._zone,
    };
  }

  private async _testCollision(pos: Vector) {
    // Grab collision data for tiles we care about
    const { tile_size, map_width, map_height } = this._mapInfo;
    const left = Math.floor(Math.max(0, pos.x / tile_size));
    const right = Math.floor(
      Math.min((pos.x + this._collider.width) / tile_size, map_width)
    );
    const top = Math.floor(Math.max(0, pos.y / tile_size));
    const bottom = Math.floor(
      Math.min((pos.y + this._collider.height) / tile_size, map_height)
    );

    // For each of the tiles we need to check
    const pipeline = this._redis.pipeline();
    for (let x = left; x <= right; x++) {
      for (let y = top; y <= bottom; y++) {
        pipeline.hget(`zone:${this._zone}:collision`, `${x}:${y}`);
      }
    }

    try {
      const tiles = await pipeline.exec();
      return (
        tiles &&
        tiles.reduce(
          (collision, [, result]) => collision || result === 'true',
          false
        )
      );
    } catch (e: any) {
      return false;
    }
  }

  // Getters/Setters
  public get uid() {
    return this._uid;
  }

  public get zone() {
    return this._zone;
  }

  public get position() {
    return this._position;
  }

  public get velocity() {
    return this._velocity;
  }
  public set velocity(value) {
    this._velocity = value;
  }

  public get facing(): Facing {
    return this._facing;
  }
  public set facing(value: Facing) {
    this._facing = value;
  }
}
