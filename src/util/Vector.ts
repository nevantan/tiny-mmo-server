export class Vector {
  constructor(private _x: number, private _y: number) {}

  public normalize() {
    const d = this.size;

    if (d > 0) {
      return new Vector(this._x / d, this._y / d);
    } else {
      return new Vector(0, 1);
    }
  }

  get x() {
    return this._x;
  }

  set x(n: number) {
    this._x = n;
  }

  get y() {
    return this._y;
  }

  set y(n: number) {
    this._y = n;
  }

  get size() {
    return Math.hypot(this._x, this._y);
  }

  static dist(v1: Vector, v2: Vector) {
    return Math.hypot(v2.x - v1.x, v2.y - v1.y);
  }
}
