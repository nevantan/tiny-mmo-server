export enum UnauthorizedAction {
  ManipulatingCharacterId = 0,
  ManipulatingUnselectedCharacter = 1,
}

export class UnauthorizedActionError extends Error {
  private _timestamp: Date;

  constructor(
    private _type: UnauthorizedAction,
    private _userId: string,
    message: string
  ) {
    super(message);

    this._timestamp = new Date();
  }

  get type() {
    return this._type;
  }

  get userId() {
    return this._userId;
  }
}
