export const tagToString = (tag: number) => {
  let output = `${tag}`;

  if (tag < 1000) output = '0' + output;
  if (tag < 100) output = '0' + output;
  if (tag < 10) output = '0' + output;

  return output;
};
