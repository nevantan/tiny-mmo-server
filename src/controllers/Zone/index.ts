// Libraries
import { resolve } from 'path';
import { readFile } from 'fs/promises';

// Node
import { ZoneNode } from '../../nodes/Zone';

// Types
import { MobType } from '../Mob';

// Types
export interface TileData {
  x: number;
  y: number;
  id: number;
  type?: string;
}

export interface MapLayer {
  name: string;
  solid: boolean;
  positions: TileData[];
}

export interface MobData {
  type: MobType;
  x: number;
  y: number;
}

export interface MapData {
  id: string;
  tile_size: number;
  map_width: number;
  map_height: number;
  layers: MapLayer[];
  mobs: MobData[];
}

export class ZoneController {
  private _zones = new Map<string, ZoneNode>();

  constructor() {
    this._initializeMapData();
  }

  async _initializeMapData() {
    const zones = ['zone_playground'];

    const st = +new Date();
    console.log('Loading maps...');

    for (const zone of zones) {
      const data = JSON.parse(
        await readFile(resolve(__dirname, `zones/${zone}/map.json`), 'utf8')
      );
      this._zones.set(data.id, new ZoneNode(data));
    }

    const dt = +new Date() - st;
    console.log(`Maps loaded in ${dt}ms!`);
  }
}
