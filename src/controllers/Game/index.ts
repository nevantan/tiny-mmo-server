const TICK_RATE = 20;
const TICK_LENGTH = 1000 / TICK_RATE;

type UpdateCallback = (dt: number, tick: number) => void;

export class GameController {
  private _previous = +new Date();
  private _tickCount = 0;

  private _onUpdateCallbacks = new Set<UpdateCallback>();

  constructor() {
    // Start the game loop
    this._tick = this._tick.bind(this);
    this._update = this._update.bind(this);

    console.log('Starting game loop...');
    this._tick();
  }

  public onUpdate(cb: UpdateCallback) {
    this._onUpdateCallbacks.add(cb);
    return () => this._onUpdateCallbacks.delete(cb);
  }

  private _tick() {
    setTimeout(this._tick, TICK_LENGTH);
    const now = +new Date();
    const delta = (now - this._previous) / 1000;

    this._update(delta, this._tickCount);

    this._previous = now;
    this._tickCount++;
  }

  private _update(dt: number, tick: number) {
    for (const cb of this._onUpdateCallbacks) {
      cb(dt, tick);
    }
  }
}
