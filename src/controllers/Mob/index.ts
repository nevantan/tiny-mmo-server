// Libraries
import { GameController } from '../Game';
import Redis from 'ioredis';

// Node
import { MobNode } from '../../nodes/Mob';

export interface MobControllerConfig {
  id: number;
  game: GameController;
  capacity?: number;
}

export class MobController {
  private _redis = new Redis();
  private _sub = new Redis();

  private _id;
  private _capacity;

  private _mobs = new Set<MobNode>();
  private _offUpdate;

  constructor({ id, game, capacity }: MobControllerConfig) {
    // Store config
    this._id = id;
    this._capacity = capacity ?? 20;

    // Hook into game loop
    this._offUpdate = game.onUpdate(this._update.bind(this));

    // Setup pubsub listeners
    this._sub.subscribe('ZONE_SHUTDOWN', () => {});
    this._sub.on('message', this._handleRedis);
  }

  private _update(dt: number, tick: number) {
    // Check for unclaimed mob nodes
    this._claimMobs();

    // Loop through nodes and make sure they're all updated
    for (const mob of this._mobs) {
      mob.update(dt, tick);
    }
  }

  private async _claimMobs() {
    // Only claim new mobs if we still have capacity
    if (this._mobs.size >= this._capacity) return;

    // Attempt to grab a new mob from redis
    const newMob = await this._redis.spop('unclaimed.mobs');

    // If we didn't get anything, there aren't any unclaimed mobs
    if (!newMob) return;

    // Parse the data and spawn a new node
    const newMobData = JSON.parse(newMob);
    this._mobs.add(
      new MobNode({
        id: `${this._id}:${this._mobs.size}`,
        ...newMobData,
      })
    );
  }

  private _handleRedis(channel: string, message: string) {
    switch (channel) {
      case 'ZONE_SHUTDOWN':
        return this._handleZoneShutdown(message);
    }
  }

  // When a zone shuts down, clear its mob nodes
  private _handleZoneShutdown(zone: string) {
    this._mobs = new Set([...this._mobs].filter((mob) => mob.zone !== zone));
  }
}
