// Libraries
import { createServer } from 'https';
import { WebSocket, WebSocketServer } from 'ws';
import { GameController } from '../Game';

// Node
import { PlayerNode } from '../../nodes/Player';

// Packets
import { Packet } from '../../objects/Packet';

export class PlayerController {
  private _wss: WebSocketServer;
  private _players: Map<string, PlayerNode> = new Map<string, PlayerNode>();

  private _offUpdate;

  constructor(game: GameController) {
    // Setup websocket server
    const server = createServer();

    this._wss = new WebSocketServer({ port: 8080 });
    console.log(`Websockets listening on port 8080...`);
    this._wss.on('connection', this._handleConnection.bind(this));

    // Hook into game loop
    this._offUpdate = game.onUpdate(this._update.bind(this));
  }

  private _update(dt: number, tick: number) {
    for (const [id, playerNode] of this._players) {
      playerNode.update(dt, tick);
    }
  }

  private _handleConnection(ws: WebSocket) {
    let pn = new PlayerNode(ws);

    console.log('Client connected! Waiting for authentication...');

    // Clients only have 5 seconds after connecting to authenticate
    const disconnectTimeout = setTimeout(() => {
      pn.off('authenticated', clientAuthed);
      ws.close(1008, 'Did not authenticate in time.');
    }, 5000);

    const clientAuthed = (uid: string) => {
      // Successful authentication
      console.log(`[${uid}] Client authenticated!`);

      // Stop the "kick if no auth" timer
      clearTimeout(disconnectTimeout);

      // Stop listening for more auth events
      pn.off('authenticated', clientAuthed);

      // Setup other node listeners we care about
      this._setupNodeListeners(uid, pn);

      // Add the node to the list of authenticated nodes
      this._players.set(uid, pn);
    };

    // Listen for the node to notify the controller of successful auth
    pn.on('authenticated', clientAuthed.bind(this));
  }

  private _setupNodeListeners(uid: string, node: PlayerNode) {
    node.on('disconnect', this._handleDisconnect.bind(this));

    console.log(`[${uid}] Listening for broadcasts`);
    node.on('broadcast', this._handleBroadcast.bind(this));
  }

  private async _handleDisconnect(uid: string) {
    // Get a reference to the node
    const node = this._players.get(uid);

    // If we didn't find a node with this ID, it's already gone for some reason,
    // don't bother with the rest of cleanup
    if (!node) return;

    // Wait for the node to clean up
    await node.cleanup();

    // Remove from player map
    this._players.delete(uid);
    console.log(`[${uid}] Node removed.`);
  }

  private _handleBroadcast(packet: Packet) {
    for (const [id, node] of this._players) {
      node.send(packet);
    }
  }
}
