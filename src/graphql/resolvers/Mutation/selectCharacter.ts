// Libraries
import Redis from 'ioredis';

// Objects
import { Character } from '../../../objects/Character';

// Types
import { MutationResolvers } from '../../types';

export const selectCharacter: MutationResolvers['selectCharacter'] = async (
  _root,
  { uid },
  { dataSources, user }
) => {
  // If not logged in, fail out
  if (!user) throw new Error('You must be logged in to select a character');

  // Fetch character data from the database
  const characterData = await dataSources.character.getMyByUID({
    uid,
    playerId: user?.id ?? '',
  });

  // If no character found, fail out
  if (!characterData) throw new Error('Invalid character selected');

  // Assemble the character object to make it easier to parse out individual fields
  const character = new Character(characterData);

  // Setup redis connection
  const redis = new Redis();

  // Note the user's active character id
  await redis.set(`user:${user.id}:active_character`, character.uid);

  // Add the character data to redis
  await redis.hset(`character:${character.uid}`, character.serialize());

  // Add the character to the zone they're currently in
  await redis.sadd(`zone:${character.zone}:characters`, character.uid);

  // Notify existing players that this player just joined the zone
  const { player, ...remoteData } = characterData;
  redis.publish(
    'REMOTE_JOINED',
    JSON.stringify({
      remoteJoined: remoteData,
    })
  );

  return {
    ...characterData,
    player: {
      ...characterData.player,
    },
  };
};
