import { GraphQLError } from 'graphql';
import { MutationResolvers } from '../../types';

export const login: MutationResolvers['login'] = (
  _root,
  { email, password },
  { dataSources }
) => {
  try {
    return dataSources.login.authenticate(email, password);
  } catch (e) {
    throw new GraphQLError('Invalid username/password combination', {
      extensions: {
        code: 'FORBIDDEN',
      },
    });
  }
};
