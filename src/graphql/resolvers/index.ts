// Libraries
import Redis from 'ioredis';

// Objects
import { Character } from '../../objects/Character';

// Resolvers
import * as Mutation from './Mutation';
import * as Query from './Query';
import * as Subscription from './Subscription';

export const resolvers = {
  Mutation,
  Query,
  Subscription,
};
