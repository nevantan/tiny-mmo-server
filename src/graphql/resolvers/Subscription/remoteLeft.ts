// Libraries
import Redis from 'ioredis';
import { withFilter } from 'graphql-subscriptions';

// Types
import { TinyMMOContext } from '../../../index';

export const remoteLeft = {
  resolve: (payload: { remoteLeft: { uid: number; zone: string } }) => {
    return payload.remoteLeft.uid;
  },
  subscribe: withFilter(
    (_root: {}, _args: {}, { redis }: TinyMMOContext) =>
      redis.asyncIterator('REMOTE_LEFT'),
    async (
      payload: { remoteLeft: { uid: number; zone: string } },
      args: {},
      { dataSources, user }: TinyMMOContext
    ) => {
      // Grab our character UID from Redis
      const redis = new Redis();
      const uid = await redis.get(`user:${user?.id}:active_character`);

      // If no active character, fail out
      if (!uid) return false;

      // Get the active character
      const character = await dataSources.character.getMyByUID({
        uid: parseInt(uid),
        playerId: user?.id ?? '',
      });

      // If we don't get a character back, something is wrong, don't send subscriptions
      if (!character) return false;

      // If my character's uid matches the remote, this is us leaving, ignore
      if (character.uid === payload.remoteLeft.uid) return false;

      // If we're in the same zone, notify of leaving
      return character.zone === payload.remoteLeft.zone;
    }
  ),
};
