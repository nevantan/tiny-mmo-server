// Libraries
import Redis from 'ioredis';
import { withFilter } from 'graphql-subscriptions';

// Types
import { TinyMMOContext } from '../../../index';

export const remoteJoined = {
  subscribe: withFilter(
    (_, __, { redis }: TinyMMOContext) => redis.asyncIterator('REMOTE_JOINED'),
    async (
      payload: {
        remoteJoined: {
          uid: number;
          zone: string;
        };
      },
      args: {},
      { dataSources, user }: TinyMMOContext
    ) => {
      // Grab our character UID from Redis
      const redis = new Redis();
      const uid = await redis.get(`user:${user?.id}:active_character`);

      // If no active character, fail out
      if (!uid) return false;

      // Get the active character
      const character = await dataSources.character.getMyByUID({
        uid: parseInt(uid),
        playerId: user?.id ?? '',
      });

      // If we don't get a character back, something is wrong, don't send subscriptions
      if (!character) return false;

      // If my character's uid matches the remote, this is us joining, ignore
      if (character.uid === payload.remoteJoined.uid) return false;

      // If my character's zone matches the remote, send it
      return character.zone === payload.remoteJoined.zone;
    }
  ),
};
