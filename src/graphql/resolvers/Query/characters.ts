import { QueryResolvers } from '../../types';

export const characters: QueryResolvers['characters'] = async (
  _root,
  _vars,
  { dataSources, user }
) => {
  const characters = await dataSources.character.getByUser(user?.id ?? '');

  return characters.map((character) => ({
    ...character,
    player: {
      ...character.player,
      characters: [],
    },
  }));
};
