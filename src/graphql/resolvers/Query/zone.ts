// Libraries
import Redis from 'ioredis';

// Types
import { QueryResolvers, MobType } from '../../types';

export const zone: QueryResolvers['zone'] = async (
  _root,
  { id },
  { dataSources, user }
) => {
  // If not logged in, error out
  if (!user) throw new Error('You must be logged in to view a zone');

  // Setup redis
  const redis = new Redis();

  // Get my character ID
  const myUID = await redis.get(`user:${user.id}:active_character`);
  if (!myUID) throw new Error('You do not have a character in the world');

  // Populate my character
  const character = await dataSources.character.getByUID(parseInt(myUID));

  // If my character is not in the current zone, error out
  if (!character || character.zone !== id)
    throw new Error('You cannot view a zone you are not within');

  // Get a list of character IDs in zone
  const characterIds = await redis.smembers(`zone:${id}:characters`);
  const characters = [];

  // For each character, not including my own
  for (const id of characterIds.filter((id) => id !== myUID)) {
    characters.push(await dataSources.character.getByUID(parseInt(id)));
  }

  // Get all mobs in the zone from redis
  const mobIds = await redis.smembers(`zone:${id}:mobs`);
  const mobs = [];
  for (const mobId of mobIds) {
    const mobData = await redis.hgetall(`mob:${mobId}`);

    mobs.push({
      id: mobData.id,
      type: mobData.type as MobType,
      zone: id,
      position: {
        x: parseInt(mobData.x),
        y: parseInt(mobData.y),
      },
      name: mobData.name,
    });
  }

  return {
    id,
    characters: characters ?? [],
    mobs: mobs ?? [],
  };
};
