import { QueryResolvers } from '../../types';

export const character: QueryResolvers['character'] = (
  _root,
  { uid },
  { dataSources, user }
) => dataSources.character.getMyByUID({ uid, playerId: user?.id ?? '' });
