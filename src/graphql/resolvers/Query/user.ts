import { QueryResolvers } from '../../types';

export const user: QueryResolvers['user'] = (_root, { id }, { dataSources }) =>
  dataSources.user.getById(id);
