import { createClient } from '@urql/core';
import jwt from 'jsonwebtoken';

const client = createClient({
  url: 'http://localhost:4000/graphql',
  fetchOptions: () => {
    const token = jwt.sign({ sub: 'server' }, process.env.JWT_SECRET ?? '');
    return {
      headers: {
        authorization: token ? `Bearer ${token}` : '',
      },
    };
  },
});

export default client;
