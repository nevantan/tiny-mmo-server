import { Vector2 } from '../types';
import { UserModel } from './UserModel';

export type CharacterModel = {
  id: string;
  uid: number;
  name: string;
  tag: number;
  taggedName: string;

  zone: string;
  position: Vector2;

  player?: UserModel;
};
