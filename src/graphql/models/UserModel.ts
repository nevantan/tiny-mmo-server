import { CharacterModel } from './CharacterModel';

export type UserModel = {
  id: string;
  email: string;
  characters?: CharacterModel[];
};
