import { GraphQLResolveInfo } from 'graphql';
import { UserModel } from './models/UserModel';
import { CharacterModel } from './models/CharacterModel';
import { TinyMMOContext } from '../index';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type BarValue = {
  __typename?: 'BarValue';
  current: Scalars['Int'];
  total: Scalars['Int'];
};

export type Character = {
  __typename?: 'Character';
  id: Scalars['ID'];
  name: Scalars['String'];
  player?: Maybe<User>;
  position: Vector2;
  tag: Scalars['Int'];
  taggedName: Scalars['String'];
  uid: Scalars['Int'];
  zone: Scalars['String'];
};

export type Mob = {
  __typename?: 'Mob';
  id: Scalars['ID'];
  name: Scalars['String'];
  position: Vector2;
  type: MobType;
  zone: Scalars['String'];
};

export enum MobType {
  GreenSlime = 'GreenSlime'
}

export type Mutation = {
  __typename?: 'Mutation';
  login: Scalars['String'];
  selectCharacter: Character;
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationSelectCharacterArgs = {
  uid: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  character?: Maybe<Character>;
  characters: Array<Maybe<Character>>;
  user?: Maybe<User>;
  zone?: Maybe<Zone>;
};


export type QueryCharacterArgs = {
  uid: Scalars['Int'];
};


export type QueryUserArgs = {
  id: Scalars['ID'];
};


export type QueryZoneArgs = {
  id: Scalars['ID'];
};

export type Subscription = {
  __typename?: 'Subscription';
  mobSpawn: Mob;
  remoteJoined: Character;
  remoteLeft: Scalars['Int'];
};

export type User = {
  __typename?: 'User';
  characters: Array<Maybe<Character>>;
  email: Scalars['String'];
  id: Scalars['ID'];
};

export type Vector2 = {
  __typename?: 'Vector2';
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type Zone = {
  __typename?: 'Zone';
  characters: Array<Maybe<Character>>;
  id: Scalars['ID'];
  mobs: Array<Maybe<Mob>>;
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  BarValue: ResolverTypeWrapper<BarValue>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Character: ResolverTypeWrapper<CharacterModel>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Mob: ResolverTypeWrapper<Mob>;
  MobType: MobType;
  Mutation: ResolverTypeWrapper<{}>;
  Query: ResolverTypeWrapper<{}>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Subscription: ResolverTypeWrapper<{}>;
  User: ResolverTypeWrapper<UserModel>;
  Vector2: ResolverTypeWrapper<Vector2>;
  Zone: ResolverTypeWrapper<Omit<Zone, 'characters'> & { characters: Array<Maybe<ResolversTypes['Character']>> }>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  BarValue: BarValue;
  Boolean: Scalars['Boolean'];
  Character: CharacterModel;
  ID: Scalars['ID'];
  Int: Scalars['Int'];
  Mob: Mob;
  Mutation: {};
  Query: {};
  String: Scalars['String'];
  Subscription: {};
  User: UserModel;
  Vector2: Vector2;
  Zone: Omit<Zone, 'characters'> & { characters: Array<Maybe<ResolversParentTypes['Character']>> };
}>;

export type BarValueResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['BarValue'] = ResolversParentTypes['BarValue']> = ResolversObject<{
  current?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  total?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type CharacterResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Character'] = ResolversParentTypes['Character']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  player?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  position?: Resolver<ResolversTypes['Vector2'], ParentType, ContextType>;
  tag?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  taggedName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  uid?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  zone?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MobResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Mob'] = ResolversParentTypes['Mob']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  position?: Resolver<ResolversTypes['Vector2'], ParentType, ContextType>;
  type?: Resolver<ResolversTypes['MobType'], ParentType, ContextType>;
  zone?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MutationResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  login?: Resolver<ResolversTypes['String'], ParentType, ContextType, RequireFields<MutationLoginArgs, 'email' | 'password'>>;
  selectCharacter?: Resolver<ResolversTypes['Character'], ParentType, ContextType, RequireFields<MutationSelectCharacterArgs, 'uid'>>;
}>;

export type QueryResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  character?: Resolver<Maybe<ResolversTypes['Character']>, ParentType, ContextType, RequireFields<QueryCharacterArgs, 'uid'>>;
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryUserArgs, 'id'>>;
  zone?: Resolver<Maybe<ResolversTypes['Zone']>, ParentType, ContextType, RequireFields<QueryZoneArgs, 'id'>>;
}>;

export type SubscriptionResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = ResolversObject<{
  mobSpawn?: SubscriptionResolver<ResolversTypes['Mob'], "mobSpawn", ParentType, ContextType>;
  remoteJoined?: SubscriptionResolver<ResolversTypes['Character'], "remoteJoined", ParentType, ContextType>;
  remoteLeft?: SubscriptionResolver<ResolversTypes['Int'], "remoteLeft", ParentType, ContextType>;
}>;

export type UserResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = ResolversObject<{
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Vector2Resolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Vector2'] = ResolversParentTypes['Vector2']> = ResolversObject<{
  x?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  y?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ZoneResolvers<ContextType = TinyMMOContext, ParentType extends ResolversParentTypes['Zone'] = ResolversParentTypes['Zone']> = ResolversObject<{
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  mobs?: Resolver<Array<Maybe<ResolversTypes['Mob']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = TinyMMOContext> = ResolversObject<{
  BarValue?: BarValueResolvers<ContextType>;
  Character?: CharacterResolvers<ContextType>;
  Mob?: MobResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  User?: UserResolvers<ContextType>;
  Vector2?: Vector2Resolvers<ContextType>;
  Zone?: ZoneResolvers<ContextType>;
}>;

