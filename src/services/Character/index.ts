import { createClient } from 'edgedb';
import e from '../../edgeql-js';

export class CharacterService {
  private _client;

  constructor() {
    this._client = createClient();
  }

  async getByUID(uid: number) {
    const character = await e
      .select(e.Character, (character) => ({
        id: true,
        uid: true,
        name: true,
        tag: true,
        zone: true,
        positionX: true,
        positionY: true,
        player: {
          id: true,
          email: true,
        },
        filter: e.op(character.uid, '=', uid),
      }))
      .run(this._client);

    if (!character) return null;
    const { positionX, positionY, name, tag, ...rest } = character;

    return {
      ...rest,
      name,
      tag,
      taggedName: `${name}#${this._padded(tag)}`,
      position: {
        x: positionX,
        y: positionY,
      },
    };
  }

  async getMyByUID({ uid, playerId }: { uid: number; playerId: string }) {
    if (!playerId) return null;

    const character = await e
      .select(e.Character, (character) => ({
        id: true,
        uid: true,
        name: true,
        tag: true,
        zone: true,
        positionX: true,
        positionY: true,
        player: {
          id: true,
          email: true,
        },
        filter: e.op(
          e.op(character.uid, '=', uid),
          'and',
          e.op(character.player.id, '=', e.uuid(playerId))
        ),
      }))
      .run(this._client);

    if (!character) return null;
    const { positionX, positionY, name, tag, ...rest } = character[0];

    return {
      ...rest,
      name,
      tag,
      taggedName: `${name}#${this._padded(tag)}`,
      position: {
        x: positionX,
        y: positionY,
      },
    };
  }

  async getByUser(playerId: string) {
    if (!playerId)
      throw new Error('You must be logged in to access your characters.');

    const characters = await e
      .select(e.Character, (character) => ({
        id: true,
        uid: true,
        name: true,
        tag: true,
        zone: true,
        positionX: true,
        positionY: true,
        player: {
          id: true,
          email: true,
        },
        filter: e.op(character.player.id, '=', e.uuid(playerId)),
      }))
      .run(this._client);

    return characters.map(
      ({ positionX, positionY, name, tag, ...character }) => ({
        ...character,
        name,
        tag,
        taggedName: `${name}#${this._padded(tag)}`,
        position: {
          x: positionX,
          y: positionY,
        },
      })
    );
  }

  private _padded(tag: number) {
    let output = `${tag}`;

    if (tag < 1000) output = '0' + output;
    if (tag < 100) output = '0' + output;
    if (tag < 10) output = '0' + output;

    return output;
  }
}
