import { createClient } from 'edgedb';
import e from '../../edgeql-js';
import argon2 from 'argon2';
import * as jwt from 'jsonwebtoken';

export class LoginService {
  private _client;

  constructor() {
    this._client = createClient();
  }

  async authenticate(email: string, password: string) {
    const user = await e
      .select(e.User, (user) => ({
        id: true,
        email: true,
        password: true,
        filter: e.op(user.email, '=', email),
      }))
      .run(this._client);

    if (!user) throw new Error('Invalid username/password combination');

    if (await argon2.verify(user.password, password)) {
      return jwt.sign({ sub: user.id }, process.env.JWT_SECRET ?? '');
    } else {
      throw new Error('Invalid username/password combination');
    }
  }
}
