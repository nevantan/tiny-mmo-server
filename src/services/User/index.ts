import { createClient } from 'edgedb';
import e from '../../edgeql-js';

export class UserService {
  private _client;

  constructor() {
    this._client = createClient();
  }

  async getById(id: string) {
    return e
      .select(e.User, (user) => ({
        id: true,
        email: true,
        filter: e.op(user.id, '=', e.uuid(id)),
      }))
      .run(this._client);
  }
}
