// Libraries
import Redis from 'ioredis';

// Types
import { MapData } from '../../controllers/Zone';

export class ZoneNode {
  private _redis;
  private _collisionMap = new Map<string, boolean>();

  constructor(private _data: MapData) {
    console.log(`[${_data.id}] Initializing zone...`);

    // Fill in the collision map from the solid layers
    for (const layer of _data.layers.filter((layer) => layer.solid)) {
      for (const tile of layer.positions) {
        this._collisionMap.set(`${tile.x}:${tile.y}`, true);
      }
    }

    // Once we've calculated the collision map, dump it into redis
    this._redis = new Redis();
    this._redis.hset(
      `zone:${_data.id}:collision`,
      Object.fromEntries(this._collisionMap)
    );

    // Also add some metadata to redis
    this._redis.hset(`zone:${_data.id}:info`, {
      map_width: this._data.map_width,
      map_height: this._data.map_height,
      tile_size: this._data.tile_size,
    });

    // Add mob spawns to the unclaimed queue so they can be instantiated by the mob controller
    this._redis.sadd(
      'unclaimed.mobs',
      ..._data.mobs.map((mob) =>
        JSON.stringify({
          zone: _data.id,
          ...mob,
        })
      )
    );

    console.log(`[${_data.id}] Ready!`);
  }
}
