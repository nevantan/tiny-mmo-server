// Libraries
import Redis from 'ioredis';
import { v4 as uuid } from 'uuid';

// Data
import mobData from './data.json';

// Types
import { MobType } from '../../graphql/types';

// Util
import { Vector } from '../../util/Vector';

interface MobStats {
  health: number;
}

interface MobData {
  id: string;
  type: MobType;
  name?: string;
  x: number;
  y: number;
  zone: string;

  stats?: Partial<MobStats>;
}

export class MobNode {
  private _redis = new Redis();

  protected _id;
  protected _type;

  protected _name;
  protected _aggroRange;
  protected _leashRange;
  protected _attackRange;

  protected _spawn;
  protected _position;
  protected _velocity = new Vector(0, 0);
  protected _lastVelocity = new Vector(0, 0);
  protected _zone;

  protected _stats: MobStats;

  protected _threat = new Map<number, number>();
  protected _target?: {
    id: number;
    position: Vector;
  };

  constructor({ id, type, name, x, y, zone, stats }: MobData) {
    // Load up mob data
    this._id = id;
    this._type = type;

    this._name = name ?? mobData[type].name;
    this._aggroRange = mobData[type].aggroRange;
    this._leashRange = mobData[type].leashRange;
    this._attackRange = mobData[type].attackRange;
    this._speed = mobData[type].speed;

    this._spawn = new Vector(x, y);
    this._position = new Vector(x, y);
    this._zone = zone;

    this._stats = {
      ...mobData[type].stats,
      ...stats,
    };

    // Calculate initial aggro table
    this._updateThreatTable();

    console.log(
      `Spawned ${this._type} in ${this._zone} at (${this._spawn.x}, ${this._spawn.y})`
    );

    // Load the data into redis
    this._redis.sadd(`zone:${this._zone}:mobs`, this._id);
    this._redis.hset(`mob:${this._id}`, {
      id: this._id,
      type: this._type,
      name: this._name,
      x: this._spawn.x,
      y: this._spawn.y,
      zone: this._zone,
      stats: JSON.stringify(this._stats),
    });

    // Push to the mob spawn subscription to notify clients of a new mob in the zone
    this._redis.publish(
      'MOB_SPAWN',
      JSON.stringify({
        id: this._id,
        type: this._type,
        name: this._name,
        position: {
          x: this._spawn.x,
          y: this._spawn.y,
        },
        zone: this._zone,

        stats: this._stats,
      })
    );
  }

  public async update(dt: number, tick: number) {
    // Update threat table
    await this._updateThreatTable();

    // If highest threat target is different than current target, increase enrage
    const newTarget = this._getHighestThreat();
    if (newTarget) {
      if (this._target && newTarget !== this._target.id) {
        // TODO: Increase enrage, force path recalculation
      }

      // Target highest-threat target
      const posData = await this._redis.hmget(
        `character:${newTarget}`,
        'positionX',
        'positionY'
      );
      const position = new Vector(parseInt(posData[0]), parseInt(posData[1]));
      this._target = {
        id: newTarget,
        position,
      };

      // If in range, attack
      if (
        Vector.dist(this._target.position, this._position) <= this._attackRange
      ) {
        console.log('Attacking target', this._target.id);
        this._velocity = new Vector(0, 0);
      } else {
        // Else
        // - If path is X frames stale, recalculate it
        // - Path towards highest-threat target
        if (this._position.x < this._target.position.x) this._velocity.x = 1;
        else this._velocity.x = -1;

        if (this._position.y < this.target.position.y) this._velocity.y = 1;
        else this._velocity.y = -1;

        // If the new velocity is different, broadcast that
        if (
          this._velocity.x !== this._lastVelocity.x ||
          this._velocity.y !== this._lastVelocity.y
        ) {
          this._lastVelocity = new Vector(this._velocity.x, this._velocity.y);
          this._redis.publish(
            'MOB_MOVE',
            JSON.stringify({
              id: this._id,
              velocity: {
                x: this._velocity.x,
                y: this._velocity.y,
              },
            })
          );
        }

        this._position.x += this._velocity.x * this._speed * dt;
        this._position.y += this._velocity.y * this._speed * dt;
      }
    }
  }

  private async _updateThreatTable() {
    // Get all the characters in the zone from redis
    const charIds = await this._redis.smembers(`zone:${this._zone}:characters`);

    for (const charId of charIds) {
      // Fetch the character position
      const uid = parseInt(charId);
      const posData = await this._redis.hmget(
        `character:${uid}`,
        'positionX',
        'positionY'
      );
      const position = new Vector(parseInt(posData[0]), parseInt(posData[1]));

      // Grab the current threat so we can manipulate it
      let threat = this._threat.get(uid) ?? 0;

      // If the character is within aggro range, add passive threat
      if (Vector.dist(position, this._position) <= this._aggroRange) {
        threat += 5;
      } else if (Vector.dist(position, this._position) > this._leashRange) {
        // If the character is out of leash range, drop all threat
        threat = 0;
      }

      // Apply the calculated threat to the table
      this._threat.set(uid, threat);
    }
  }

  private _getHighestThreat() {
    let target;
    let highestThreat = 0;

    for (const [uid, threat] of this._threat) {
      if (threat > highestThreat) {
        highestThreat = threat;
        target = uid;
      }
    }

    return target;
  }

  // Getter/setter
  get zone() {
    return this._zone;
  }
}
