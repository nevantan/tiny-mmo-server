// Libraries
import { WebSocket } from 'ws';
import { EventEmitter } from 'node:events';
import jwt from 'jsonwebtoken';
import Redis from 'ioredis';
import { Vector } from '../../util/Vector';

// Objects
import {
  PacketType,
  Packet,
  AuthPacket,
  AuthSuccessPacket,
  LocalFacingPacket,
  RemoteFacingPacket,
  LocalVelocityPacket,
  RemoteVelocityPacket,
  KeepAlivePacket,
} from '../../objects/Packet';
import { Character } from '../../objects/Character';
import {
  UnauthorizedActionError,
  UnauthorizedAction,
} from '../../util/Errors/UnauthorizedActionError';

export class PlayerNode extends EventEmitter {
  private _redis;
  private _subscriptions;

  // Handlers
  private _keepAliveInterval: NodeJS.Timer;
  private _keepAliveTimeout: NodeJS.Timeout | null = null;

  // Player info
  private _id?: string;
  private _email?: string;

  // Character
  private _character?: Character;

  private _lastBroadcastFacing = new Vector(0, 0);
  private _lastBroadcastVelocity = new Vector(0, 0);
  private _lastPositionBroadcast = 0;

  constructor(private _ws: WebSocket) {
    super();

    // Setup redis for data access and publishing messages
    this._redis = new Redis();

    // Setup redis subscription instance and subscribe to data we're interested in
    this._subscriptions = new Redis();
    this._subscriptions.subscribe('zone.');

    // Listen for websocket messages from the client
    this._ws.on('message', this._handleMessage.bind(this));

    // Listen for the websocket being closed by the client
    this._ws.on('close', this._handleDisconnect.bind(this));

    // Start a keep-alive timer (5 seconds)
    this._keepAliveInterval = setInterval(this._keepAlive.bind(this), 5000);
  }

  public send(packet: Packet) {
    this._ws.send(packet.serialize());
  }

  // On each server tick
  public update(dt: number, tick: number) {
    // If there is no active character, no need to update
    if (!this._character) return;

    // Simulate the next tick for the character
    this._character.update(dt, tick);

    // Update redis data with new character pos
    this._redis.hmset(
      `character:${this._character.uid}`,
      'positionX',
      Math.round(this._character.position.x),
      'positionY',
      Math.round(this._character.position.y)
    );

    // Broadcast facing direction
    if (
      this._character.facing.x !== this._lastBroadcastFacing.x ||
      this._character.facing.y !== this._lastBroadcastFacing.y
    ) {
      this.emit(
        'broadcast',
        new RemoteFacingPacket(this._character.facing, this._character.uid)
      );
      this._lastBroadcastFacing = new Vector(
        this._character.facing.x,
        this._character.facing.y
      );
    }

    // Broadcast velocity + position
    if (
      this._character.velocity.x !== this._lastBroadcastVelocity.x ||
      this._character.velocity.y !== this._lastBroadcastVelocity.y
    ) {
      this.emit(
        'broadcast',
        new RemoteVelocityPacket(
          this._character.uid,
          this._character.velocity,
          this._character.position
        )
      );
      this._lastBroadcastVelocity = new Vector(
        this._character.velocity.x,
        this._character.velocity.y
      );
    }

    // Set an authoritative position packet every second (20 ticks)
    if (tick - this._lastPositionBroadcast >= 20) {
      this.emit(
        'broadcast',
        new RemoteVelocityPacket(
          this._character.uid,
          this._character.velocity,
          this._character.position
        )
      );
      this._lastPositionBroadcast = tick;
    }
  }

  // Perform any cleanup needed so this node can be removed
  public async cleanup() {
    console.log(`[${this._id}] Cleaning up...`);

    // Remove the keep alive loop
    clearInterval(this._keepAliveInterval);

    // Remove any dangling timeouts
    if (this._keepAliveTimeout) clearTimeout(this._keepAliveTimeout);
  }

  // Called when it is time to send a keepalive packet
  private async _keepAlive() {
    // console.log(`[${this._id}] Send keepalive...`);

    // Send the packet to the client
    this.send(new KeepAlivePacket(true));

    // Set a timeout to disconnect the client if we don't hear back in time (5 seconds)
    this._keepAliveTimeout = setTimeout(() => {
      // If this code runs, we didn't hear back from the client in time - disconnect
      console.log(`[${this._id}] Timed out`);
      this._handleDisconnect();
    }, 5000);
  }

  // Args, in case we need in future:
  // (
  //   socket: WebSocket,
  //   code: number,
  //   reason: string
  // )
  private async _handleDisconnect() {
    console.log(`[${this._id}] Disconnected from ${this._character?.zone}`);

    // Remove redis data
    await this._redis.del(
      `user:${this._id}:active_character`,
      `character:${this._character?.uid}`
    );
    await this._redis.srem(
      `zone:${this._character?.zone}:characters`,
      `${this._character?.uid ?? ''}`
    );

    // Broadcast that this character is going away
    await this._redis.publish(
      'REMOTE_LEFT',
      JSON.stringify({
        remoteLeft: {
          uid: this._character?.uid,
          zone: this._character?.zone,
        },
      })
    );

    // Let the player controller know we've disconnected so it can delete this node
    this.emit('disconnect', this._id);
  }

  private _handleMessage(data: Buffer) {
    try {
      const type = data.readUInt8(0);
      const payload = data.subarray(1);

      if (type === PacketType.Auth) {
        this._handleAuthPacket(AuthPacket.parse(payload));
      } else if (type === PacketType.LocalFacing) {
        this._handleLocalFacingPacket(LocalFacingPacket.parse(payload));
      } else if (type === PacketType.LocalVelocity) {
        this._handleLocalVelocityPacket(LocalVelocityPacket.parse(payload));
      } else if (type === PacketType.KeepAlive) {
        this._handleKeepAlivePacket(KeepAlivePacket.parse(payload));
      }
    } catch (e: any) {
      console.error(e.message);
    }
  }

  private async _handleAuthPacket(packet: AuthPacket) {
    try {
      // Make sure the token they sent over is good
      const decoded = jwt.verify(packet.token, process.env.JWT_SECRET ?? '');

      // If we haven't thrown at this point, auth was good
      this._id = decoded.sub as string;

      // Load in the data we need from Redis

      // Get the selected character from Redis (this was set by the selectCharacter mutation)
      const charId = await this._redis.get(
        `user:${decoded.sub}:active_character`
      );

      // Fetch the player data from Redis and store it in memory (also set by selectCharacter)
      const charData = await this._redis.hgetall(`character:${charId}`);
      this._character = new Character({
        id: charData.id,
        uid: parseInt(charData.uid),
        name: charData.name,
        tag: parseInt(charData.tag),
        position: {
          x: parseInt(charData.positionX),
          y: parseInt(charData.positionY),
        },
        zone: charData.zone,
      });

      // Notify the client that everything has been loaded and is ready to go
      this.send(new AuthSuccessPacket());

      // Notify the parent controller of successful authentication
      this.emit('authenticated', decoded.sub);
    } catch (e: any) {
      this._ws.close(1008, 'Authentication failed!');
      console.error('Auth failed:', e.message);
    }
  }

  private _handleLocalFacingPacket(packet: LocalFacingPacket) {
    if (!this._character) {
      throw new UnauthorizedActionError(
        UnauthorizedAction.ManipulatingUnselectedCharacter,
        this._id ?? '',
        'Attempted to change facing direction while there is no character selected'
      );
    }

    this._character.facing = packet.facing;
  }

  private _handleLocalVelocityPacket(packet: LocalVelocityPacket) {
    if (!this._character) {
      throw new UnauthorizedActionError(
        UnauthorizedAction.ManipulatingUnselectedCharacter,
        this._id ?? '',
        'Attempted to change velocity direction while there is no character selected'
      );
    }

    this._character.velocity = new Vector(packet.velocity.x, packet.velocity.y);
  }

  private _handleKeepAlivePacket(packet: KeepAlivePacket) {
    // console.log(`[${this._id}] Keep alive acknowledged`);

    // When we get a keep alive packet back from the client,
    // clear the disconnect timeout if it exists so we don't get disconnected
    if (this._keepAliveTimeout) clearTimeout(this._keepAliveTimeout);
  }
}
